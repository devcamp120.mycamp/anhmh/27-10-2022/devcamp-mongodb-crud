//Import bộ thư viện Express
const { response } = require('express');
const express = require('express');

//Import Course Middleware
const { printCourseURLMiddleware } = require("../middlewares/courseMiddleware");
//Import module course controllers
const { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById } = require('../controllers/courseController');

const router = express.Router();

//Get a course
router.get("/courses", printCourseURLMiddleware, getAllCourses);

//Create a course
router.post("/courses", printCourseURLMiddleware, createCourse);

//Get a course by Id
router.get("/courses/:courseId", printCourseURLMiddleware, getCourseById);

//Update a course
router.put("/courses/:courseId", printCourseURLMiddleware, updateCourseById);

//Delete a course
router.delete("/courses/:courseId", printCourseURLMiddleware, deleteCourseById);

//Export dữ liệu thành 1 module
module.exports = router;