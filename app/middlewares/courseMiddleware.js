const printCourseURLMiddleware = (request, response, next) => {
    console.log("Course URL: " + request.url);
    next();
}

module.exports = {
    printCourseURLMiddleware: printCourseURLMiddleware
}