//Import course model
const { default: mongoose } = require('mongoose');
const courseModel = require('../models/courseModel');

//Khai báo các Actions:
//Get all course
const getAllCourses = (request, response) => {
    //Bước 1: Thu thập dữ liệu (bỏ qua)
    //Bước 2: Kiểm tra dữ liệu (bỏ qua)
    //Bước 3: Thực hiện thao tác dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                message: `Internal server error: ${error.message}`
            })
        } else {
            response.status(200).json({
                data
            })
        }
    })
};

//Get a course by Id
const getCourseById = (request, response) => {
    //Bước 1: Thu thập dữ liệu (bỏ qua)
    let id = request.params.courseId;
    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3: Thực hiện thao tác dữ liệu
        courseModel.findById(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        })
    }
};

//Create a course
const createCourse = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!body.title) {
        response.status(400).json({
            message: "Title is required!"
        })
    } else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        response.status(400).json({
            message: "noStudent is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let course = {
            _id: mongoose.Types.ObjectId(),
            title: body.title,
            description: body.description,
            noStudent: body.noStudent
        }
        courseModel.create(course, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(201).json({
                    data
                })
            }
        });
    }
};

//Update a course
const updateCourseById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.courseId;
    let body = request.body;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else if (!body.title) {
        response.status(400).json({
            message: "Title is required!"
        })
    } else if (!Number.isInteger(body.noStudent) || body.noStudent < 0) {
        response.status(400).json({
            message: "noStudent is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        let course = {
            title: body.title,
            description: body.description,
            noStudent: body.noStudent
        }
        courseModel.findByIdAndUpdate(id, course, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(200).json({
                    data
                })
            }
        });
    }
};

//Delete a course
const deleteCourseById = (request, response) => {
    //Bước 1: Thu thập dữ liệu
    let id = request.params.courseId;

    //Bước 2: Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(id)) {
        response.status(400).json({
            message: "Id is invalid!"
        })
    } else {
        //Bước 3: Thực hiện các thao tác nghiệp vụ
        courseModel.findByIdAndDelete(id, (error, data) => {
            if (error) {
                response.status(500).json({
                    message: `Internal server error: ${error.message}`
                })
            } else {
                response.status(204).json({
                    data
                })
            }
        })
    }
};
//Export các hàm ra module
module.exports = { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById }
