//Bước 1: Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Bước 2: Khai báo thư viện Schema
const Schema = mongoose.Schema;

//Bước 3: Tạo ra 1 đối tượng Schema tương ứng với 1 collection trong mongodb
const reviewSchema= new Schema({
    //_id : mongoose.Types.ObjectId,
    _id: {
        Types: mongoose.Types.ObjectId
    },
    stars: {
        Types: Number,
        default: 0
    },
    note: {
        Types: String,
        required: false
    }
});

//Bước 4: Export ra 1 model cho schema
module.exports = mongoose.model("Review", reviewSchema);