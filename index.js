//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khai báo thư viện Monoose
const mongoose = require('mongoose');

//Import Router Sử Dụng Middlewares
const courseRouter = require("./app/routes/courseRouter");
const reviewRouter = require("./app/routes/reviewRouter");

//Khai báo các model
const reviewModel = require("./app/models/reviewModel");

//Kết nối đến cơ sở dữ liệu Mongodb
mongoose.connect('mongodb://localhost:27017/CRUD_Course', (error) => {
    if (error) {
        throw error;
    }
    console.log("Successfully connected!");
});

//Khởi tạo app express
const app = new express();

//Sử dụng được body json
app.use(express.json());

//Sử dụng body unicode
app.use(express.urlencoded({
    extended:true
}))

//Khai báo cổng của project 
const port = 8000;

// app.use((request, response, next) => {
//     console.log("Time", new Date());
//     next();
// })

// app.use((request, response, next) => {
//     console.log("Request method: ", request.method);
//     next();
// })

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Xin chào! Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

app.use("/", courseRouter);
app.use("/", reviewRouter);

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})